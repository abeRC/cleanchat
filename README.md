# CleanCHAT

Script for cleaning CHAT transcription files for the TalkBank databases.

## Useful Links

- [TalkBank](https://talkbank.org/)
- [SLP's Guide to CLAN](https://talkbank.org/manuals/Clin-CLAN.pdf)
- [CHAT Manual](https://talkbank.org/manuals/CHAT.pdf)
- [TalkBank on GitHub](https://github.com/TalkBank)

## ToDos

- [ ] GUI (candidates: Kivy, Tkinter, PySimpleGUI, wxPython, Gooey)
- [ ] Insert missing colon at lines beginning with `*` or `%`.
- [x] Err lines that doesn't match the pattern `^(\*[A-Z]{3}|%[a-z]{3}):`.
- [x] Warn lines beginning with 3+ uppercase letters, or 3+ (lowercase) letters followed by a colon.
- [ ] Automatically create `@ID` lines from participant-specific lines.
- [ ] New option `--anonymize`
- [ ] New option `--pseudonyms`
- [x] Option to exclude files from cleaning.
- [x] Change linebreaks to LF.
- [x] New option to allow user to choose how to replace `#`:
  - short pause `(.)`
  - medium pause `(..)`
  - long pause `(...)`
  - don't change
