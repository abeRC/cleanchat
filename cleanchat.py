import re
from argparse import ArgumentParser
from os import listdir, replace
from tempfile import NamedTemporaryFile

if __name__ != '__main__':
    raise Exception("cleanchat isn't a module, it must be called "
                    "independently!")

PAUSES = {'SHORT': '(.)', 'MEDIUM': '(..)', 'LONG': '(...)', 'NONE': None}

def parseArguments():
    parser = ArgumentParser(description = 'Clean .cha files',
                            epilog = 'cleanchat v0, Felipe Oliveira da Silva '
                            'Netto')
    parser.add_argument('input_files', metavar='FILE', nargs='*',
                        help='zero or more CHAT files as input. When missing, '
                        'it uses all .cha files from working directory.')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Verbose output')
    parser.add_argument('-o', dest='out_pattern', metavar='FILE_PATTERN',
                        default='%f.cha', help='Output file name (in case of '
                        'one input file) or output filename pattern. For a '
                        'pattern, %%f denotes the corresponding input '
                        'filename (w/o extension), and %%%% denotes the %% '
                        'character. When missing, it overwrites the input '
                        'files.')
    parser.add_argument('--change-hashes', metavar='PAUSE', default=None,
                        help="Replace '#' according to PAUSE. PAUSE can be "
                        "'short' for short pause symbol '" + PAUSES['SHORT'] +
                        "', 'medium' for medium pause symbol '" +
                        PAUSES['MEDIUM'] + "', 'long' for long pause symbol '" +
                        PAUSES['LONG'] + "' or None for no replacement. Default"
                        " is None.")
    parser.add_argument('-e', '--exclude', metavar='FILE', nargs='+',
                        help='Exclude FILEs from input file list, so not '
                        'cleaning them. It might come preferably after '
                        'positional arguments.')

    args = parser.parse_args()
    if len(args.input_files) == 0:
        args.input_files = list(filter(lambda x: x[-4:] == '.cha', listdir()))
    if args.exclude:
        for file in args.exclude:
            print(file)
            try:
                args.input_files.remove(file)
            except ValueError as e:
                del e
        del args.exclude

    args.out_pattern = args.out_pattern.split('%%')

    if args.change_hashes:
        args.change_hashes = args.change_hashes.upper()
        if args.change_hashes not in PAUSES:
            raise Exception('Unvalid value to option --change-hashes')
        args.change_hashes = PAUSES[args.change_hashes]

    return args

pattern1 = re.compile('\s+') # Match any amount of whitespace.
pattern2 = re.compile('(?<=^.)\s+') # Match whitespace preceded by any character, but only if that character is at the start of the string. '?<=' is a positive lookbehind, which doesn't include what it matches in the result.
pattern3 = re.compile('^(\*[A-Z]{3}|%[a-z]{3}):') # Match three-letter codes (plus colon).
pattern3warn = re.compile('^([A-z]{3,}):') # Match bigger-than-normal three-letter codes (plus colon).
pattern4 = re.compile('^\w+:') # Match one or more word characters at the start of the string (plus colon).

def cleanFile(filename, args):
    with open(filename) as file:
        if args.verbose:
            print('Cleaning', filename + '...')
        filename = filename.split('.')[0]
        outname = '%'.join(map(lambda x: x.replace('%f', filename),
                           args.out_pattern))
        with NamedTemporaryFile(mode='w', newline='\n', suffix='.cha', dir='.',
                                delete=False) as outfile:
            line_no = 0
            for line in file:
                if line.isspace():
                    continue
                line = line.strip()
                line = pattern1.sub(' ', line) # Replace whitespace with a single space.
                if args.change_hashes:
                    line = line.replace('#', args.change_hashes)
                line = line.replace(',,', '„') # Replace double comma.

                if line[0] in ('@', '*', '%'):
                    line = pattern2.sub('', line) # Eliminate space (if any) between the symbol and the code that follows it.
                    if line[:4].upper() == '@END':
                        outfile.write('@End\n')
                        break
                    lines = line.split(':')
                    lines[0] = lines[0].rstrip()
                    if len(lines) > 1:
                        lines[1] = '\t' + lines[1].lstrip()
                    line = ':'.join(lines)
                    if not line[0] is "@" and not pattern3.match(line):
                        print(f"ERR: Invalid code at line {line_no+1}!")
                elif pattern3warn.match(line):
                    if pattern3warn.match(line).group().isupper():
                        print(f"WRN: Maybe there is a * missing at line {line_no+1}?")
                    elif pattern3warn.match(line).group().islower():
                        print(f"WRN: Maybe there is a % missing at line {line_no+1}?")
                    else:
                        print(f"WRN: Maybe there is a % or * missing at line {line_no+1}? (Could not determine which due to mixed case.)")
                else:
                    line = '\t' + line
                outfile.write(line + '\n')
                
                line_no += 1
            outfile.close()
            replace(outfile.name, outname)
        if args.verbose:
            print('saved in', outname)

args = parseArguments()

for fname in args.input_files:
    cleanFile(fname, args)
